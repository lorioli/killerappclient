import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class TopMenuNav extends Component {
  logout = () => {
    localStorage.clear();
    window.location.reload()
  }
  render() {
    return (<nav>
      <ul>
        <li>Edit Profile</li>
        <li>My Played Games</li>
        <li onClick={() => this.props.handleChangeIndex(3)}>Hall Of Fame</li>
        <li>Game Rules</li>
        <Link to='/'><li onClick={() => this.logout()}>Log Out</li></Link>
      </ul>
    </nav >)
  }
}

export default TopMenuNav
