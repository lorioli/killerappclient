import React from "react";
import Slide from "@material-ui/core/Slide";

const Modal = (props) => {
  return (
    <div className="modal">
      <div className="modal-backdrop" />
      <Slide direction="up" in={true} mountOnEnter unmountOnExit>
        <div className="modal-contentBox">{props.children}</div>
      </Slide>
    </div>
  );
}

export default Modal