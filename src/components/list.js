import React, { Component } from "react";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import { Link, withRouter } from "react-router-dom";
import Highlighter from "react-highlight-words";
import Flag from "@material-ui/icons/Flag";
import GpsNotFixed from "@material-ui/icons/GpsNotFixed";

class List extends Component {
    constructor(props) {
        super(props);

        this.state = {
            charToMatch: "",
            data: props.data || [],
            filteredList: props.data || [],
            type: "games"
        };
    }
    handleChange = event => {
        this.setState({ charToMatch: event.target.value });
    };

    findMatches = (charToMatch, givenArray) => {
        return givenArray.filter(item => {
            const regex = new RegExp(charToMatch, "gi");
            return item.name.match(regex);
        });
    };
    filterList = (event = "") => {
        event = ""
            ? this.setState({ charToMatch: "", filteredList: this.state.data })
            : this.setState({
                charToMatch: event.target.value,
                filteredList: this.state.data.filter(item => {
                    const regex = new RegExp(event.target.value, "gi");
                    return item.name.match(regex) || item.creatorName.match(regex);
                })
            });
    };
    // componentDidUpdate(prevProps, prevState) {
    //     if (this.props !== prevProps) {
    //         this.setState({ data: this.props.data, type: this.props.type });
    //     }
    //     if (this.state.data !== prevState.data) {
    //         this.setState({ filteredList: this.state.data, type: this.state.type });
    //     }
    // }
    render() {
        return (
            <section className="listScreen">
                <Input
                    type="search"
                    id="gameSearch"
                    label="Search field"
                    placeholder="Search"
                    value={this.state.charToMatch}
                    onChange={this.filterList}
                />
                <ul className={`list list-${this.state.type}`}>
                    {this.state.type === "games" && (
                        <li className="listHeader">
                            <span>pl.</span>
                            <span>game name</span>
                            <span>creator</span>
                            <span>type</span>
                        </li>
                    )}
                    {this.state.filteredList.map(item => (
                        <li key={item.gameId || item.userId}>
                            {this.state.type === "games" && <span>{item.maxPlayers}</span>}
                            <Link to={`gamesList/${item.gameId}`}>
                                <Highlighter
                                    highlightClassName="highlight"
                                    searchWords={[this.state.charToMatch]}
                                    autoEscape={true}
                                    textToHighlight={item.name}
                                /></Link>
                            <Highlighter
                                highlightClassName="highlight"
                                searchWords={[this.state.charToMatch]}
                                autoEscape={true}
                                textToHighlight={item.creatorName}
                            />
                            {this.state.type === "games" && (
                                <span>
                                    {item.type === 0 && <GpsNotFixed />}
                                    {item.type === 1 && <Flag />}
                                </span>
                            )}
                        </li>
                    ))}
                </ul>
                <Link to="/createGame">
                    <Button className='createGameBtn' variant="fab" color="primary">
                        <AddIcon />
                    </Button>
                </Link>
            </section>
        );
    }
}

export default withRouter(List);
