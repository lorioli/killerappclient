import React, { Component } from "react";
import { Input, Button } from "@material-ui/core";
import Highlighter from "react-highlight-words";
import axios from 'axios'
import GameDetails from './gamedetails'
import Modal from './modal'
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import { Link } from 'react-router-dom'

class GameWoUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            charToMatch: "",
            data: props.data || [],
            type: "users",
            successModalIsVisible: false
        };
    }
    handleChange = event => {
        this.setState({ charToMatch: event.target.value });
    };

    findMatches = (charToMatch, givenArray) => {
        return givenArray.filter(item => {
            const regex = new RegExp(charToMatch, "gi");
            return item.name.match(regex);
        });
    };
    filterList = (event = "") => {
        event = ""
            ? this.setState({ charToMatch: "", filteredList: this.state.gameUsers })
            : this.setState({
                charToMatch: event.target.value,
                filteredList: this.state.gameUsers.filter(item => {
                    const regex = new RegExp(event.target.value, "gi");
                    return item.username.match(regex);
                })
            });
    };
    fetchGameInfos = () => {
        axios
            .get(`${this.props.apiUrl}/games/${this.props.gameId}/users`)
            .then(res => {
                const gameUsers = res.data;
                this.setState({ gameUsers });
            });
    };
    joinGame = () => {
        const data = {
            userId: this.props.userId,
            gameId: this.props.gameId
        }
        axios
            .post(`${this.props.apiUrl}/games/${this.props.gameId}/users`, { data })
            .then(this.setState({ successModalIsVisible: true }))
            .catch(error => error);
    }
    componentDidMount() {
        this.fetchGameInfos();
    }
    render() {
        const { filteredList, charToMatch, gameUsers } = this.state
        const { name } = this.props.data
        if (gameUsers) {
            const valid = filteredList ? filteredList : gameUsers
            console.log(valid)
            return (
                <React.Fragment>
                    <section className="listScreen list-gameUsers">
                        <GameDetails name={name}></GameDetails>
                        <Input
                            type="search"
                            id="gameSearch"
                            label="Search field"
                            placeholder="Search"
                            value={charToMatch}
                            onChange={this.filterList}
                        />
                        <ul className={`list list-${this.state.type}`}>
                            {valid.map((item, index) => (
                                <li key={item.userId || index}>
                                    <Highlighter
                                        highlightClassName="highlight"
                                        searchWords={[charToMatch]}
                                        autoEscape={true}
                                        textToHighlight={item.username || ''}
                                    />
                                </li>
                            ))}
                        </ul>
                        <Button variant="contained" color="primary" style={{ marginTop: 60 }} onClick={() => this.joinGame()}>Join Game</Button>
                        <Button variant="outlined" color="primary" style={{ marginTop: 20 }} onClick={() => window.history.back()}>Back</Button>
                    </section>
                    {this.state.successModalIsVisible && (
                        <Modal in={true}>
                            <CheckCircleOutline style={{ fontSize: 120 }} color="primary" />
                            <h1>Success!</h1>
                            <h3>
                                You've been added to the game <br />
                                Watch for the start!
                            </h3>
                            <Link to="/" onClick={this.props.fetchGames}>
                                <Button variant="contained" color="primary">
                                    Back to Hub
                                </Button>
                            </Link>
                        </Modal>
                    )}
                </React.Fragment>
            );
        } else return null
    }
}

export default GameWoUser;
