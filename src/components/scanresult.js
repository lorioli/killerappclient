import React from 'react';


const ScanResult = (props) => {
    if (props.playerStatus !== undefined) {
        console.log(props.playerStatus)
        return (
            <div className='scanResult'>
                <img src={require(`../assets/scan_result/${props.playerStatus}.png`)} alt="player status" />
                {props.playerStatus !== 4 && <h3>+1</h3>}
                <h2 className="scanTitle">{props.playerStatus === 1 ? 'YOU WIN' : props.playerStatus === 2 ? 'YOU WIN' : props.playerStatus === 3 ? 'YOU WIN' : 'YOU LOOSE'}</h2>
            </div>
        );
    }
    else return null
}

export default ScanResult