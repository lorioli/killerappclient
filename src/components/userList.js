import React, { Component } from "react";

class UserList extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <section className="listScreen">
                <ul className={`list list-users`}>
                    {this.props.data !== undefined && this.props.data.map(item => (
                        <li key={item.userId} className={item.userId === this.props.userId ? 'activeUser' : ''} >
                            {item.username}
                        </li>
                    ))}

                </ul>
            </section>
        );
    }
}

export default UserList;
