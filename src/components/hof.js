import React, { Component } from 'react'

class Hof extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: []
        }
    }
    componentDidMount() {
        this.setState({ data: this.props.data })
    }
    render() {
        const user = this.props.user !== undefined ? this.props.user : ''
        return (
            <div>
                <div className="topMenu-hof" >
                    <h1>Hall Of Fame</h1>
                    <ul className='list list-hof'>
                        <li className="listHeader">
                            <span>username</span>
                            <span>kills</span>
                        </li>
                        {this.state.data.map((item, index) => (
                            <li key={index} className={item.username === user.username ? 'activeUser' : ''}><span>{item.username}</span><span>{item.kills}</span></li>
                        ))}
                    </ul>
                    <p className="backLink" onClick={() => this.props.handleChangeIndex(0)}>&laquo; back to menu</p>
                </div >
            </div>
        )
    }
}
export default Hof;