import React, { Component } from "react";
import GameQRScreen from "./gameqrscreen";
import GameScanScreen from "./gamescanscreen";
import UserList from "./userList";
import GameDetails from "./gamedetails";
import axios from "axios";
import Button from "@material-ui/core/Button";
import PlayerStatus from "./playerstatus";
import Modal from "./modal";
import { Link } from "react-router-dom";
import DeleteForever from "@material-ui/icons/DeleteForever";
import Warning from "@material-ui/icons/Warning"

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gameDetailsVisible: true,
      scanIsVisible: false,
      statusIsVisible: false,
      deleteModalIsVisible: false,
      deleted: false
    };
  }
  startGame = () => {
    const data = {
      gameId: this.props.data.gameId
    };
    return axios
      .patch(`${this.props.apiUrl}/games`, { data })
      .then(this.props.fetchGames)
      .catch(error => error);
  };
  deleteGame = () => {
    console.log('delete')
    return axios
      .delete(`${this.props.apiUrl}/games/${this.props.data.gameId}`)
      .then(this.setState({ deleted: true }))
      .catch(error => error);
  };
  toggleScanMode = () => {
    this.setState({
      scanIsVisible: !this.state.scanIsVisible,
      statusIsVisible: false
    });
  };
  toggleStatusMode = () => {
    this.setState({
      statusIsVisible: !this.state.statusIsVisible,
      scanIsVisible: false
    });
  };
  toggleDeleteModal = () => {
    this.setState({ deleteModalIsVisible: !this.state.deleteModalIsVisible })
  }
  // sendScanData = (param) => {
  //   console.log(param)
  //   const scannedData = param.split('/')
  //   const data = {
  //     gameId: this.props.data.gameId,
  //     scannedPlayerId: Number(scannedData[0]),
  //     scanningPlayerId: this.props.userId
  //   };
  //   return axios
  //     .patch(`${this.props.apiUrl}/games/${this.props.data.gameId}`, { data })
  //     .then(() => {
  //       this.props.fetchGames;
  //       this.fetchGameInfos();
  //     })
  //     .catch(error => error);
  // };
  scanComplete = () => {
    console.log('scanComplete invoked in parent')
    // this.sendScanData(data)
    this.setState({ scanIsVisible: false })
  }
  fetchGameInfos = () => {
    console.log('fetchGames called')
    axios
      .get(`${this.props.apiUrl}/games/${this.props.data.gameId}/users`)
      .then(res => {
        const gameUsers = res.data;
        const playerStatus = gameUsers.filter(
          item => item.userId === this.props.userId
        )[0].playerStatus;
        const playerKills = gameUsers.filter(
          item => item.userId === this.props.userId
        )[0].gameKills;
        this.setState({ gameUsers, playerStatus, playerKills });
      });
  };
  componentDidMount() {
    //If game is running then fetch playerStatus every ms
    if (this.props.data.gameStatus === 2) {
      this.intervalId = setInterval(() => {
        console.log("fetched");
        this.fetchGameInfos();
      }, 2500);
    }
    this.fetchGameInfos();
  }
  // componentDidUpdate(prevProps) {
  //   if (this.props.data.gameStatus !== prevProps.data.gameStatus) {
  //     this.fetchGameInfos()
  //     if (this.props.data.gameStatus === 2 && !this.state.scanIsVisible) {
  //       this.intervalId = setInterval(() => {
  //         console.log("fetched");
  //         this.fetchGameInfos();
  //       }, 30000);
  //     }
  //   }
  // }
  componentWillUnmount() {
    clearInterval(this.intervalId);
  }
  render() {
    const {
      gameStatus,
      gameId,
      name,
      players,
      minPlayers,
      authorId
    } = this.props.data;
    const { userId, user, fetchGames, apiUrl } = this.props;
    const {
      gameDetailsVisible,
      gameUsers,
      scanIsVisible,
      playerStatus,
      statusIsVisible,
      playerKills
    } = this.state;
    return (
      <section className="game">
        {gameDetailsVisible && <GameDetails name={name} />}
        {gameStatus < 2 && (
          <div className="game-notStarted">
            <UserList
              data={gameUsers}
              userId={userId}
              type="users"
              withFilter={false}
            />
            {userId === authorId && (
              <Button onClick={() => this.startGame()} disabled={players.length < minPlayers} variant="contained" color="primary">Start Game</Button>
            )}
            {userId === authorId && (
              <Button onClick={() => this.toggleDeleteModal()} variant="outlined" color="secondary">Delete Game</Button>
            )}
          </div>
        )}
        {gameStatus === 2 &&
          !scanIsVisible &&
          !statusIsVisible &&
          playerStatus !== 4 && (
            <GameQRScreen userId={userId} user={user} gameId={gameId} />
          )}
        {gameStatus === 2 && !scanIsVisible &&
          !statusIsVisible && playerStatus === 4 &&
          <PlayerStatus
            playerStatus={playerStatus}
            playerKills={playerKills}
          />}
        {scanIsVisible && <GameScanScreen scanComplete={this.scanComplete} playerStatus={playerStatus} userId={userId} gameId={gameId} apiUrl={apiUrl} fetchGames={fetchGames} fetchGamesInfos={this.fetchGamesInfos} />}
        {gameUsers !== undefined &&
          statusIsVisible && (
            <PlayerStatus
              playerStatus={playerStatus}
              playerKills={playerKills}
            />
          )}
        {gameStatus === 2 &&
          playerStatus !== 4 && (
            <div className="actionBtns">
              <div className="btn btn-blue" onClick={() => this.toggleStatusMode()}>My Status</div>
              <div className="btn btn-red" onClick={() => this.toggleScanMode()}>Kill</div>{/* //TODO: add touchstart and touchend events */}
            </div>
          )}
        {gameStatus === 3 && "Game Finished screen"}
        {this.state.deleteModalIsVisible && (
          <Modal in={true}>
            {!this.state.deleted &&
              <div><Warning style={{ fontSize: 120 }} color="secondary" /><h3>Are you sure you want to <b>permanently</b> delete this game ?</h3>
                <Button onClick={() => this.toggleDeleteModal()} >Cancel</Button>
                <Button variant="contained" color="secondary" onClick={() => this.deleteGame()} >Confirm</Button></div>}
            {this.state.deleted && <div><DeleteForever style={{ fontSize: 120 }} color="primary" />
              <h3>
                Your game was successfully deleted <br />
              </h3>
              <Link to="/gamesList" onClick={this.props.fetchGames}>
                <Button variant="contained" color="primary">
                  Back to Hub
              </Button>
              </Link></div>}
          </Modal>
        )}
      </section>
    );
  }
}

export default Game;
