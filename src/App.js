import React, { Component } from "react";
import AppMain from "./components/appmain";
import Login from "./components/login";
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import { Router } from "react-router";
import createBrowserHistory from "history/createBrowserHistory";
import axios from "axios";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

const history = createBrowserHistory(this.props);

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#33c4b3',
      contrastText: '#fff'
    }
  },
  overrides: {
    MuiButton: {
      // Name of the styleSheet
      contained: {
        // Name of the rule
        background: "#33c4b3",
        borderRadius: 50,
        border: 0,
        color: "#fff",
        padding: "10px 20px",
        minWidth: '140px',
        boxShadow: "0 0 0"
      },
      outlined: {
        // Name of the rule
        background: "none",
        borderRadius: 50,
        border: '1px solid #33c4b3',
        padding: "10px 20px",
        minWidth: '140px',
        boxShadow: "0 0 0"
      }
    }
  }
});

class App extends Component {
  constructor() {
    super();

    this.state = {
      isLoggedIn: localStorage.getItem('isLoggedIn'),
      userId: null,
      user: [],
      apiUrl: 'https://f90def38.eu.ngrok.io/api' //"http://10.1.107.4:3000/api"
    };
  }
  componentDidMount() {
    const userId = Number(localStorage.getItem('userId'))
    this.setState({ userId })
    axios.get(`${this.state.apiUrl}/users/${userId}`).then(res => {
      const user = res.data;
      this.setState({ user });
    });
  }
  render() {
    const { userId, user, apiUrl, isLoggedIn } = this.state;
    if (typeof userId === 'number') {
      return (
        <Router history={history} >
          <MuiThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <div className="App">
                {isLoggedIn ? (
                  <AppMain userId={userId} user={user[0]} apiUrl={apiUrl} forceUpdate={() => this.forceUpdate()} />
                ) : (
                    <Login apiUrl={apiUrl} forceUpdate={() => this.forceUpdate()} />
                  )}
              </div>
            </MuiPickersUtilsProvider>
          </MuiThemeProvider>
        </Router>
      )
    } else return null
  }
}

export default App;
