import React, { Component } from "react";
import QRCode from "qrcode.react";

export default class gameQrScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: props.user || []
    };
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.props !== prevProps) {
      this.setState({ user: this.props.user });
    }
  }
  render() {
    const { userId, gameId } = this.props;
    const { user } = this.state;
    return (
      <section className="gameQrScreen">
        <QRCode
          className="QRCode"
          value={`${userId}/${gameId}`}
          size={200}
          fgColor="#000"
          bgColor="#ffffff"
          level="L"
          renderAs="svg"
        />
        <p>{user.username}</p>
      </section>
    );
  }
}
