import React from 'react'
import { Link } from 'react-router-dom'

const Tab = (props) => {
    const { index, value, userGames, handleChange } = props
    const corrValue = value === 0 ? 1 : value
    if (userGames[1] || value !== 0) {
        return (
            < Link to={`/`} className={`tab ${index === value && 'active'} ${userGames[corrValue - 1] || 'empty'}`
            } onClick={() => { userGames[corrValue - 1] && handleChange(value) }}> {userGames[value] ? userGames[value].name.charAt(0) : userGames[value - 1] ? '+' : ''}</Link >)
    }
    return (<Link to="/" className={`tab ${index === value && 'active'}`} onClick={() => handleChange(value)}>{userGames[0] ? userGames[0].name.charAt(0) : '+'}</Link>)
}

export default Tab