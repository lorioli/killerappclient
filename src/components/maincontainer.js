import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import CreateGame from "./creategame";
import List from "./list";
import Game from "./game";
import Tab from './tab'
import GameWoUser from './gamewouser'

class MainContainer extends Component {
  // this.props.games + this.props.userGames
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      games: [],
      userGames: props.userGames || []
    }
  }
  handleChangeIndex = param => {
    this.setState({ index: param })
  }
  render() {
    const { apiUrl, userId, userGames, games, user } = this.props;
    const { index } = this.state
    return (
      <section className="mainContainer">
        <div className="tabBar">
          <Tab value={0} index={index} userGames={userGames} handleChange={this.handleChangeIndex} />
          <Tab value={1} index={index} userGames={userGames} handleChange={this.handleChangeIndex} />
          <Tab value={2} index={index} userGames={userGames} handleChange={this.handleChangeIndex} />
          <Tab value={3} index={index} userGames={userGames} handleChange={this.handleChangeIndex} />
          <Tab value={4} index={index} userGames={userGames} handleChange={this.handleChangeIndex} />
        </div>

        <Switch>
          <Route
            path="/createGame"
            render={props => <CreateGame userId={userId} apiUrl={apiUrl} fetchGames={this.props.fetchGames.bind(this)} />}
          />
          <Route path='/games/:id' render={(props) => {
            const filteredGames = userGames.filter(item => item.gameId == props.match.params.id)
            return (<Game user={user} fetchGames={this.props.fetchGames} userId={userId} apiUrl={apiUrl} gameId={props.match.params.id} data={filteredGames[0]} />)
          }}
          />
          <Route exact path='/' render={() => userGames[index] ? <Redirect to={`/games/${userGames[index].gameId}`} /> : <List data={games} type="games" />} />
          <Route
            exact path="/gamesList"
            render={props => <List data={games} type="games" />}
          />
          <Route
            path="/gamesList/:id"
            render={props => {
              const filteredGames = games.filter(item => item.gameId == props.match.params.id)
              return (<GameWoUser user={user} fetchGames={this.props.fetchGames} userId={userId} apiUrl={apiUrl} gameId={props.match.params.id} data={filteredGames[0]} />)
            }}
          />
        </Switch>
      </section >
    );
  }
}

export default MainContainer;
