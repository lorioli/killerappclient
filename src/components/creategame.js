import React, { Component } from "react";
import Radio from "@material-ui/core/Radio";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Modal from "./modal";
import Button from "@material-ui/core/Button";
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";
import DatePicker from "material-ui-pickers/DatePicker";
import axios from "axios";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import { Link } from "react-router-dom";

class CreateGame extends Component {
  // TODO: error handling on fields and Validation
  constructor() {
    super();

    this.state = {
      name: "",
      gameType: 0,
      gameDuration: "11:30",
      gameDurationChecked: false,
      gameDate: new Date(),
      gameDateChecked: false,
      minPlayers: "",
      maxPlayers: "",
      successModalIsVisible: false
    };
  }
  handleDateChange = date => {
    this.setState({ gameDate: date });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleToggle = param => event => {
    this.setState({ [param]: event.target.checked });
  };
  handleSubmit = event => {
    event.preventDefault();
    const data = {
      name: this.state.name,
      gameType: this.state.gameType,
      minPlayers: this.state.minPlayers,
      maxPlayers: this.state.maxPlayers,
      userId: this.props.userId
    };
    if (this.state.gameDateChecked) {
      Object.assign(data, { gameDate: this.state.gameDate });
    }
    if (this.state.gameDurationChecked && this.state.gameType == 1) {
      Object.assign(data, { gameDuration: this.state.gameDuration });
    }
    return axios
      .post(`${this.props.apiUrl}/games`, { data })
      .then(this.setState({ successModalIsVisible: true }))
      .catch(error => error);
  };
  render() {
    const {
      name,
      gameType,
      gameDuration,
      gameDurationChecked,
      gameDate,
      gameDateChecked,
      minPlayers,
      maxPlayers,
      postSuccess
    } = this.state;
    return (
      <React.Fragment>
        <form onSubmit={this.handleSubmit}>
          <div className="container form form-game">
            <div>
              <TextField
                required
                type="text"
                value={name}
                onChange={this.handleChange}
                label="Game Name"
                name="name"
                min="5"
                max="200"
                placeholder="choose a name"
              />
            </div>
            <div>
              <InputLabel>Frag</InputLabel>
              <Radio
                checked={gameType == 0}
                value="0"
                onChange={this.handleChange}
                name="gameType"
                aria-label="Frag"
              />
              <Radio
                checked={gameType == 1}
                value="1"
                onChange={this.handleChange}
                name="gameType"
                aria-label="Flag"
              />
              <InputLabel>Flag</InputLabel>
            </div>
            <div>
              <Checkbox
                disabled={gameType == 0}
                onChange={this.handleToggle("gameDurationChecked")}
                checked={gameDurationChecked}
                value="gameDurationChecked"
              />
              <TextField
                disabled={gameType == 0}
                label="Duration"
                id="duration"
                type="time"
                name="gameDuration"
                value={gameDuration}
                onChange={this.handleChange}
              />
            </div>
            <div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Checkbox
                  onChange={this.handleToggle("gameDateChecked")}
                  checked={gameDateChecked}
                  value="gameDateChecked"
                />
                <DatePicker
                  disabled={!gameDateChecked}
                  format="dd/MM/yyyy"
                  label="Date"
                  value={gameDate}
                  onChange={this.handleDateChange}
                  showTodayButton
                />
              </MuiPickersUtilsProvider>
            </div>
            <div className="players">
              <TextField
                required
                value={minPlayers}
                onChange={this.handleChange}
                label="Min Players"
                name="minPlayers"
                min="5"
                max="200"
                placeholder="5"
              />
              <TextField
                required
                value={maxPlayers}
                onChange={this.handleChange}
                label="Max Players"
                name="maxPlayers"
                min="5"
                max="200"
                placeholder="10"
              />
            </div>
            <Button type="submit" variant="contained" color="primary">
              Create Game
            </Button>
            <Button variant="outlined" color="primary" style={{ marginTop: 20 }} onClick={() => window.history.back()}>Back</Button>
          </div>
        </form>
        {this.state.successModalIsVisible && (
          <Modal in={true}>
            <CheckCircleOutline style={{ fontSize: 120 }} color="primary" />
            <h1>Success!</h1>
            <h3>
              Your game was successfully created. <br />
              Have fun!
            </h3>
            <Link to="/" onClick={this.props.fetchGames}>
              <Button variant="contained" color="primary">
                Back to Hub
              </Button>
            </Link>
          </Modal>
        )}
      </React.Fragment>
    );
  }
}

export default CreateGame;
