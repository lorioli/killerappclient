import React, { Component } from 'react';
import { TextField, Button, IconButton, InputAdornment } from "@material-ui/core";
import { Visibility, VisibilityOff, CheckCircleOutline } from '@material-ui/icons';
import Modal from './modal'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Logo from '../assets/logo.png'

class Login extends Component {
    constructor() {
        super()

        this.state = {
            username: '',
            email: '',
            password: '',
            registerScreen: false,
            showPassword: false,
            successModalIsVisible: false,
            usernameAlreadyUsed: false,
            emailAlreadyUsed: false,
            loginError: false
        }
    }
    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
        if (prop === 'username') { this.setState({ usernameAlreadyUsed: false, loginError: false }) }
        if (prop === 'email') { this.setState({ emailAlreadyUsed: false, loginError: false }) }
    };
    register = () => {
        this.setState({
            registerScreen: !this.state.registerScreen, username: '',
            email: '',
            password: '',
        })
    }
    handleMouseDownPassword = event => {
        event.preventDefault();
    };
    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };
    handleRegister = (event) => {
        event.preventDefault();
        const data = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        };
        axios
            .post(`${this.props.apiUrl}/users/validation`, { data })
            .then(res => {
                if (res.data[0].length > 0 && res.data[1].length > 0) { this.setState({ usernameAlreadyUsed: true, emailAlreadyUsed: true }) }
                if (res.data[0].length > 0) { this.setState({ usernameAlreadyUsed: true }) }
                if (res.data[1].length > 0) { this.setState({ emailAlreadyUsed: true }) }
                if (res.data[0].length < 1 && res.data[1].length < 1) {
                    return 'ok'
                } else return 'notok'

            })
            .then(res => {
                if (res === 'ok') {
                    return axios
                        .post(`${this.props.apiUrl}/users`, { data })
                } else return null
            })
            .then(res => {
                localStorage.setItem('isLoggedIn', true)
                localStorage.setItem('userId', res.data.insertId)
                this.setState({ successModalIsVisible: true })
            })
            .catch(error => error);
    }
    handleLogin = (event) => {
        event.preventDefault()
        const data = {
            username: this.state.username,
            password: this.state.password,
        }
        axios
            .post(`${this.props.apiUrl}/login`, { data })
            .then(res => {
                localStorage.setItem('token', res.data.token)
                localStorage.setItem('isLoggedIn', true)
                localStorage.setItem('userId', res.data.userId)
                window.location.reload()
            })
            .catch(err => { this.setState({ loginError: true }); err })
    }
    render() {
        const { username, email, password, usernameAlreadyUsed, emailAlreadyUsed, loginError } = this.state
        return (
            <section className="login" >
                {!this.state.registerScreen && <form onSubmit={this.handleLogin}>
                    <div className='form form-login'>
                        <img className='logo' src={require(`../assets/logo.png`)} alt="logo" />
                        <div>
                            <TextField
                                required
                                type="text"
                                value={username}
                                onChange={this.handleChange('username')}
                                label="Username"
                                style={{ marginTop: 20 }}
                                error={loginError}
                            /></div>
                        <div>
                            <TextField
                                required
                                error={loginError}
                                type={this.state.showPassword ? 'text' : 'password'}
                                value={password}
                                onChange={this.handleChange('password')}
                                label="Password"
                                style={{ marginTop: 14, width: 168 }}
                                placeholder='password'
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Toggle password visibility"
                                            onClick={this.handleClickShowPassword}
                                            onMouseDown={this.handleMouseDownPassword}
                                        >
                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton></InputAdornment>,
                                }}
                            /></div><br /><span className='error'>{loginError ? 'Wrong username and/or password' : ''}</span>
                        <Button type="submit" style={{ marginTop: 40 }} variant='contained' color="primary">Login</Button>
                        <p className='register'>Not registered yet?</p>
                        <div>
                            <Button variant='outlined' color="primary" onClick={this.register}>Register</Button>
                        </div>
                    </div>
                </form >}
                {
                    this.state.registerScreen && <form onSubmit={this.handleRegister}><div className='form form-login'>
                        <img className='logo' src={require(`../assets/logo.png`)} alt="logo" />
                        <div>
                            <TextField
                                required
                                type="text"
                                value={username}
                                onChange={this.handleChange('username')}
                                label="Username"
                                style={{ marginTop: 20 }}
                                error={usernameAlreadyUsed}
                            /></div><span className='error'>{usernameAlreadyUsed ? 'Username already in use' : ''}</span>
                        <div>
                            <TextField
                                required
                                type="text"
                                value={email}
                                onChange={this.handleChange('email')}
                                label="Email"
                                style={{ marginTop: 14 }}
                                error={emailAlreadyUsed}
                            /></div><span className='error'>{emailAlreadyUsed ? 'Email already in use' : ''}</span>
                        <div>
                            <TextField
                                required
                                type={this.state.showPassword ? 'text' : 'password'}
                                value={password}
                                onChange={this.handleChange('password')}
                                label="Password"
                                style={{ marginTop: 14, width: 168 }}
                                placeholder='password'
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="Toggle password visibility"
                                            onClick={this.handleClickShowPassword}
                                            onMouseDown={this.handleMouseDownPassword}
                                        >
                                            {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton></InputAdornment>,
                                }}
                            /></div>
                        <Button type="submit" style={{ marginTop: 66 }} variant='contained' color="primary">Register</Button>
                        <div>
                            <Button variant='outlined' color="secondary" onClick={this.register}>Cancel</Button>
                        </div>
                    </div></form >
                }
                {
                    this.state.successModalIsVisible && (
                        <Modal>
                            <CheckCircleOutline style={{ fontSize: 120 }} color="primary" />
                            <h1>Success!</h1>
                            <h3>
                                Your account was successfully created <br />
                                Have fun!
                        </h3>
                            <Link to="/" onClick={() => window.location.reload()}>
                                <Button variant="contained" color="primary">
                                    Continue
                            </Button>
                            </Link>
                        </Modal>
                    )
                }
            </section >
        );
    }
}

export default Login;