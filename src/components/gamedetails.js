import React from 'react'

const GameDetails = (props) => {
    return (
        <section className="game-details">
            <h1>{props.name}</h1>
        </section>
    );
}

export default GameDetails;