import React, { Component } from 'react';
import QrReader from 'react-qr-reader';
import axios from 'axios'
import ScanResult from './scanresult'
import CircularProgress from "@material-ui/core/CircularProgress";

export default class GameScanScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            result: null,
            scanActive: true,
            waitingScreen: false,
            scanResult: false
        }

        this.handleScan = this.handleScan.bind(this)
    }
    handleScan(data) {
        if (data !== null) {
            this.setState({
                result: data,
                scanActive: false,
                waitingScreen: true,
                scanResult: false
            })
            this.sendScanData(data)
            console.log('launch timeOut')
            setTimeout(() => { this.setState({ waitingScreen: false, scanResult: true }) }, 2500)
            setTimeout(() => { console.log('2nd timer'); this.props.scanComplete() }, 7000)
        }
    }
    sendScanData = (param) => {
        console.log(param)
        const scannedData = param.split('/')
        const data = {
            gameId: this.props.gameId,
            scannedPlayerId: Number(scannedData[0]),
            scanningPlayerId: this.props.userId
        };
        return axios
            .patch(`${this.props.apiUrl}/games/${this.props.gameId}`, { data })
            .then(() => this.props.fetchGameInfos()
                // this.setState({ waitingScreen: false, scanResult: true });
                // this.props.scanComplete

            )
            .catch(error => error);
    };
    handleError(err) {
        console.error(err)
    }
    componentWillUnmount() {
        if (this.timerHandle) {
            // Yes, clear it     
            clearTimeout(this.timerHandle);
            this.timerHandle = 0;
        }
    }
    render() {
        return (
            <React.Fragment>
                <section className="gameScanScreen">
                    {this.state.scanActive && <div className="camera">
                        <QrReader className="scanCode"
                            delay={800}
                            onError={this.handleError}
                            onScan={this.handleScan} />
                        <p>{this.state.result}</p>
                        {/* <button type="button" name="back">Back</button> */}
                    </div>}
                    {this.state.waitingScreen && <div className='waitingScreen'>
                        <CircularProgress color='primary' />
                        <h2>One moment please <br />Someone is being killed</h2>
                    </div>}
                    {this.state.scanResult && <ScanResult playerStatus={this.props.playerStatus} />}
                </section >
            </React.Fragment>
        )
    }
}
