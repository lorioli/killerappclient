import React, { Component } from "react";
import MainContainer from "./maincontainer";
import TopMenu from "./topmenu";
import axios from "axios";

export default class AppMain extends Component {
  constructor() {
    super();
    this.state = {
    };
  }
  fetchGames = () => {
    console.log('fetchGames on appmain called')
    axios.get(`${this.props.apiUrl}/games`)
      .then(res => {
        const games = res.data.filter(item => (!item.players.includes(this.props.userId) && item.gameStatus < 1))
        const userGames = res.data.filter(
          game =>
            game.players !== null &&
            Array.isArray(game.players) &&
            game.players.includes(this.props.userId) && game.gameStatus < 3)
        this.setState({ games, userGames });
      });
  }
  componentDidMount() {
    this.fetchGames()
  }
  render() {
    if (this.state.games && this.state.userGames) {
      const { games, userGames } = this.state;
      const { userId, apiUrl, user } = this.props;
      return (
        <React.Fragment>
          <TopMenu user={user} apiUrl={apiUrl} forceUpdate={this.props.forceUpdate} />
          <MainContainer
            games={games}
            userGames={userGames}
            userId={userId}
            user={user}
            apiUrl={apiUrl}
            fetchGames={this.fetchGames.bind(this)}
          />
        </React.Fragment>
      )
    } else return null
  }
}
