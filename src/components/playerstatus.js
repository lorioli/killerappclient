import React from 'react';


const PlayerStatus = (props) => {
    if (props !== undefined) {
        return (
            <div className='playerStatus'>
                <img src={require(`../assets/player_status/${props.playerStatus}.png`)} alt="player status" />
                <p>{props.playerKills} kills</p>
                <h2 className="title">{props.playerStatus === 1 ? 'Prey' : props.playerStatus === 2 ? 'Killer' : props.playerStatus === 3 ? 'Spy' : 'Dead'}</h2>
            </div>
        );
    }
    else return null
}

export default PlayerStatus