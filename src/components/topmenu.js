import React, { Component } from "react";
import DragHandle from "@material-ui/icons/DragHandle";
import avatar from "../assets/avatar.png"
import LogoSmall from "../assets/logo_small.png"
import TopMenuNav from './topmenunav'
import axios from 'axios'
import Hof from './hof'

class TopMenu extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: false,
      index: 0,
      hof: []
    }
  }
  // state isOpen : true / false

  handleTouch = () => {
    this.setState({ isOpen: !this.state.isOpen, index: 0 })
  }
  handleChangeIndex = (param) => {
    this.setState({ index: param })
  }
  componentDidMount() {
    axios.get(`${this.props.apiUrl}/hof`).then(res => {
      const hof = res.data;
      this.setState({ hof });
    });
  }
  render() {
    const { isOpen, index, hof } = this.state
    const user = this.props.user !== undefined ? this.props.user : ''
    return (
      <div className={`topMenu ${isOpen && 'isOpen'}`} >
        {index === 0 && <TopMenuNav handleChangeIndex={this.handleChangeIndex} forceUpdate={this.props.forceUpdate}></TopMenuNav>}
        {/* {index === 1 && <UserList handleChangeIndex={() => this.handleChangeIndex}></UserList>} */}
        {index === 3 && <Hof user={user} data={hof} handleChangeIndex={this.handleChangeIndex} />}
        <div className='logo_small'><img src={LogoSmall} alt="logo small" /></div>
        <div className="dragBar" onClick={this.handleTouch}>
          <DragHandle />
        </div>
        <div className="avatar" onClick={this.handleTouch}>{user.username}<img className="avatarImg" src={avatar} alt="avatar" /></div>
      </div >
    );
  }
}

export default TopMenu;